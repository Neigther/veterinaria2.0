'use strict'

//Llamar las librerias
const bodyParser = require('body-parser');

const express = require('express');

//iniciamos express
const app = express();

//Activar cors
app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Request-Widht, Content-Type, Acept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next();
});
//Middlewares de express
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(require('../Routes/index'));

module.exports = app;