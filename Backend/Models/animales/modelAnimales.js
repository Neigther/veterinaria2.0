const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevoAnimal = new Schema({
    nombreAnimal: {type: String},
    especie: {type: String},
    edad: {type: Number},
    nombreDuenio: {type: String},
    apellidosDuenio: {type: String},
    telefonoDuenio: {type: Number},
    motivoConsulta: {type: String},
    fechaConsulta: {type: Date},
    fechaSalidaConsulta: {type: Date},
    costoConsulta: {type: Number},
    diagnostico: {type: String},
    
});

module.exports = mongoose.model('animales', nuevoAnimal);