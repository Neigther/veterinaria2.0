'use strict'

const mongoose = require('mongoose');

const app = require('../Server/index');
const port = 3900;
//generar promesa

mongoose.Promise = global.Promise;

//hacemos la coneccion
mongoose.connect('mongodb://localhost:27017/veterinaria', {useNewUrlParser: true})
.then(()=>{
    console.log("Base de Datos corriendo")

    //Escucha al servidor
    app.listen(port, ()=>{
        console.log(`Servidor corriendo el puerto: ${port}`);
    })

})