const express = require('express');
const nuevoAnimal = require('../../Models/animales/modelAnimales');
let app = express();

// Función para manejar la respuesta
const handleResponse = (res, success, message, data) => {
    const status = success ? 200 : 500;
    const response = success
        ? { ok: true, message, data }
        : { ok: false, message, err: data };

    return res.status(status).json(response);
};

//nuevo dato inicio de metodo post
app.post('/animal/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);
    let newSchemaAnimal = new nuevoAnimal({
        nombreAnimal: body.nombreAnimal,
        especie: body.especie,
        edad: body.edad,
        nombreDuenio: body.nombreDuenio,
        apellidosDuenio: body.apellidosDuenio,
        telefonoDuenio: body.telefonoDuenio,
        motivoConsulta: body.motivoConsulta,
        fechaConsulta: body.fechaConsulta,
        fechaSalidaConsulta: body.fechaSalidaConsulta,
        costoConsulta: body.costoConsulta,
        diagnostico: body.diagnostico
    });
    newSchemaAnimal.save()
        .then(data => {
            handleResponse(res, true, 'Datos guardados', data); // Llamada a la función de manejo de la respuesta
        })
        .catch(err => {
            handleResponse(res, false, 'Ocurrió un problema', err); // Llamada a la función de manejo de la respuesta
        });
});
//inicio metodo get

app.get('/todos/animales', async (req, res) => {
    await nuevoAnimal
        .find()
        .then(data => {
            handleResponse(res, true, 'Datos Encontrados', data); // Llamada a la función de manejo de la respuesta
        })
        .catch(err => {
            handleResponse(res, false, 'Ocurrió un problema', err); // Llamada a la función de manejo de la respuesta
        });
})

//Inicia metodo get by id
app.get('/id/animales/:id', async (req, res) => {
    await nuevoAnimal
        .findById(req.params.id)
        .then(data => {
            handleResponse(res, true, 'Datos Encontrados', data); // Llamada a la función de manejo de la respuesta
        })
        .catch(err => {
            handleResponse(res, false, 'Ocurrió un problema', err); // Llamada a la función de manejo de la respuesta
        });
})

//fin del metodo get by id

//inicio metodo put
app.put('/cambiar/animal/:id', async (req, res) => {
    const {
        nombreAnimal,
        especie,
        edad,
        nombreDuenio,
        apellidosDuenio,
        telefonoDuenio,
        motivoConsulta,
        fechaConsulta,
        fechaSalidaConsulta,
        costoConsulta,
        diagnostico
    } = req.body;

    let animalNuevo = await nuevoAnimal.findById(req.params.id)

    animalNuevo.nombreAnimal = nombreAnimal;
    animalNuevo.especie = especie;
    animalNuevo.edad = edad;
    animalNuevo.nombreDuenio = nombreDuenio;
    animalNuevo.apellidosDuenio = apellidosDuenio;
    animalNuevo.telefonoDuenio = telefonoDuenio;
    animalNuevo.motivoConsulta = motivoConsulta;
    animalNuevo.fechaConsulta = fechaConsulta;
    animalNuevo.fechaSalidaConsulta = fechaSalidaConsulta;
    animalNuevo.costoConsulta = costoConsulta;
    animalNuevo.diagnostico = diagnostico;

    animalNuevo = await nuevoAnimal.findOneAndUpdate({ _id: req.params.id }, animalNuevo, { new: true })
    .then(data => {
        handleResponse(res, true, 'Datos Actualizados', data); // Llamada a la función de manejo de la respuesta
    })
    .catch(err => {
        handleResponse(res, false, 'Ocurrió un problema', err); // Llamada a la función de manejo de la respuesta
    });
})

//inicio metodo delete
app.delete('/eliminar/animal/:id', async (req, res) => {
    let animalNuevo = await nuevoAnimal.findById(req.params.id)

    await nuevoAnimal.findOneAndRemove({ _id: req.params.id })
    .then(data => {
        handleResponse(res, true, 'Datos Borrados', data); // Llamada a la función de manejo de la respuesta
    })
    .catch(err => {
        handleResponse(res, false, 'Ocurrió un problema', err); // Llamada a la función de manejo de la respuesta
    });
})

module.exports = app;