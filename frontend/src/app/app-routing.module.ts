import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AnimalesComponent} from './/components/animales/animales.component';
import { FormularioComponent } from './components/formulario/formulario.component';

const routes: Routes = [
  {path: 'animales', component: AnimalesComponent},
  {path: 'rellenoform', component: FormularioComponent},
  {path: 'editar/:id', component: FormularioComponent},
  { path: '#', pathMatch: 'full', redirectTo: 'animales' },
  { path: '##', pathMatch: 'full', redirectTo: 'rellenoform' },
  { path: '##', pathMatch: 'full', redirectTo: 'editar/' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
