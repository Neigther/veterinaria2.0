import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { Validators } from '@angular/forms';
// Servicio
import { AnimalesService } from '../../service/animales.service';
// Componente
import { modelAnimales } from '../../../../Models/modelAnimales';
import { Router } from '@angular/router';

@Component({
  selector: 'app-animales',
  templateUrl: './animales.component.html',
  styleUrls: ['./animales.component.css'],
})
export class AnimalesComponent implements OnInit {
  public Animal: any[] = [];
  AnimalF: FormGroup;
  public id: any;
  G = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
  P = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
  I = Math.floor(Math.random() * (3 - 1 + 1)) + 1;

  constructor(
    private _AnimalesService: AnimalesService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.AnimalF = this.fb.group({
      nombreAnimal: ['', Validators.required],
      especie: ['', Validators.required],
      edad: ['', Validators.required],
      nombreDuenio: ['', Validators.required],
      apellidosDuenio: ['', Validators.required],
      telefonoDuenio: ['', Validators.required],
      motivoConsulta: ['', Validators.required],
      fechaConsulta: ['', Validators.required],
      fechaSalidaConsulta: ['', Validators.required],
      costoConsulta: ['', Validators.required],
      diagnostico: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.BuscarAnimales();
  }

  postAnimal() {
    console.log(this.Animal);
    // Recoge los datos del formulario
    const ANIMAL: modelAnimales = {
      nombreAnimal: this.AnimalF.get('nombreAnimal')?.value,
      especie: this.AnimalF.get('especie')?.value,
      edad: this.AnimalF.get('edad')?.value,
      nombreDuenio: this.AnimalF.get('nombreDuenio')?.value,
      apellidosDuenio: this.AnimalF.get('apellidosDuenio')?.value,
      telefonoDuenio: this.AnimalF.get('telefonoDuenio')?.value,
      motivoConsulta: this.AnimalF.get('motivoConsulta')?.value,
      fechaConsulta: this.AnimalF.get('fechaConsulta')?.value,
      fechaSalidaConsulta: this.AnimalF.get('fechaSalidaConsulta')?.value,
      costoConsulta: this.AnimalF.get('costoConsulta')?.value,
      diagnostico: this.AnimalF.get('diagnostico')?.value,
    };
    if (this.id !== undefined) {
      this._AnimalesService.putAnimal(this.id, ANIMAL).subscribe((data: any)=>{
      this.Actualizado();
      this.BuscarAnimales();
      this.AnimalF.reset();
      })
      
    }else if(this.id == undefined){
      this._AnimalesService.postAnimal(ANIMAL).subscribe((data: any)=>{
        this.Exito();
        this.BuscarAnimales();
        this.AnimalF.reset();
      }, error =>{
        console.log(error);
      })
    }
  }

  // Metodo Buscar Todo
  BuscarAnimales() {
    this.Animal = [];
    this._AnimalesService.getAnimales().subscribe(
      (data: any) => {
        this.Animal = data.data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  deleteAnimal(id: any) {
    Swal.fire({
      title: '¿Estas seguro de borrar este Registro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar Registro',
    }).then((result) => {
      if (result.isConfirmed) {
        this._AnimalesService.deleteAnimal(id).subscribe(
          (data) => {
            this.BuscarAnimales();
          },
          (error) => {
            console.log(error);
          }
        );
        Swal.fire('Eliminado!', 'El Registro a sido eliminado.');
      }
    });
  }

  getIDAnimal(id: any) {
    this.id = id;
    this._AnimalesService.getByIdAnimal(this.id).subscribe((data: any) => {
      this.Animal = data.data;
      console.log(this.Animal);
      this.AnimalF.setValue({
        nombreAnimal: data.data.nombreAnimal,
        especie: data.data.especie,
        edad: data.data.edad,
        nombreDuenio: data.data.nombreDuenio,
        apellidosDuenio: data.data.apellidosDuenio,
        telefonoDuenio: data.data.telefonoDuenio,
        motivoConsulta: data.data.motivoConsulta,
        fechaConsulta: data.data.fechaConsulta,
        fechaSalidaConsulta: data.data.fechaSalidaConsulta,
        costoConsulta: data.data.costoConsulta,
        diagnostico: data.data.diagnostico,
      });
      console.log("este es valor de: "+this.id)
    });
  }

  Exito() {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Registrado con Exito',
      showConfirmButton: false,
      timer: 1500,
    });
  }

  Actualizado() {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Actualizado con Exito',
      showConfirmButton: false,
      timer: 1500,
    });
  }
}
